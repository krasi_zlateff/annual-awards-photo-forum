var express = require('express');
var app = express();

// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

// set the view engine to ejs
app.set('view engine', 'ejs');

// make express look in the public directory for env (css/js/img)
app.use(express.static(__dirname + '/public'));
// app.use(express.static(__dirname + '/javascripts'));
// app.use(express.static(__dirname + '/node_modules'));

// set the home page route
app.get('/', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('index');
});

// set the register page route
app.get('/register', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('register');
});

// set the register preview photos page route
app.get('/register-preview-photos', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('register_preview_photos');
});

// set the register successfully page route
app.get('/register-successfully', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('register_successfully');
});

// set the judges page route
app.get('/judges', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('judges');
});

// set the contacts page route
app.get('/contacts', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('contacts');
});

// set the contacts page route
app.get('/participants', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('participants');
});

// set the category page route
app.get('/category', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('category');
});

// set the category page route
app.get('/blog', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('blog');
});

// set the category page route
app.get('/blog-post', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('blog_post');
});

// set the form elements route
app.get('/form-elements', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('form_elements');
});

// set the voting route
app.get('/voting', function (req, res) {
    // ejs render automatically looks in the views folder
    res.render('voting');
});

app.listen(port, function () {
    console.log('Our app is running on http://localhost:' + port);
});
